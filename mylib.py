"""
Python 3
    Web scraping using selenium to click the "Export" button
        - from https://archivist.closer.ac.uk/admin/export
"""

import pandas as pd
import time
import sys
import os
from urllib.parse import urlparse

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions


def get_driver():
    options = FirefoxOptions()
    # options.browser_version = '92'
    # options.platform_name = 'Windows 10'
    driver = webdriver.Remote(
        'http://selenium-standalone-firefox:4444/wd/hub',
        options=options)

    # locally use this instead:
    # driver = webdriver.Firefox()

    return driver

def archivist_login(driver, url, uname, pw, sleep_time=10):
    """
    Log in to archivist

    Returns:
        bool: True if the login seemed to work, else False.
    """
    driver.get(url)
    time.sleep(sleep_time)

    try:
        driver.find_element(By.ID, "email").send_keys(uname)
        driver.find_element(By.ID, "password").send_keys(pw)
        driver.find_element(By.CLASS_NAME, "MuiButton-label").click()
    except NoSuchElementException as e:
        print(e)
        print("So we give up on this host")
        return False
    time.sleep(sleep_time)
    # check next page's title to make sure one is logged in

    # if there are still login fields, then it failed
    try:
        _ = driver.find_element(By.ID, "email")
        failed = True
    except NoSuchElementException as e:
        failed = False
    if failed:
        print("failed to login to this host, check your credentials?")
        return False
    return True


def url_base(s):
    """Extract the "base" of a url.

    For example, from "https://example.com/something/somewhere.html" returns
    "https://example.com"
    """
    o = urlparse(s)
    return "{}://{}".format(o.scheme, o.netloc)



def get_base_url(df):
    """
    Return a dataframe of files that need to be downloaded from instruments export page
    """

    df["base_url"] = df.apply(lambda row: "https://closer-archivist.herokuapp.com/admin/instruments/exports" if row["Archivist"].lower() == "main"
                                     else "https://closer-archivist-" + row["Archivist"].lower() + ".herokuapp.com/admin/instruments/exports",
                                     axis=1)

    return df


if __name__ == "__main__":
    raise RuntimeError("don't run this directly")

